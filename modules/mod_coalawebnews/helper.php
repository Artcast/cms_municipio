<?php

defined("_JEXEC") or die("Restricted access");
/**
 * @package             Joomla
 * @subpackage          CoalaWeb News Module
 * @author              Steven Palmer
 * @author url          http://coalaweb.com
 * @author email        support@coalaweb.com
 * @license             GNU/GPL, see /assets/en-GB.license.txt
 * @copyright           Copyright (c) 2016 Steven Palmer All rights reserved.
 *
 * CoalaWeb News is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
jimport('joomla.filesystem.file');

require_once JPATH_SITE . '/components/com_content/helpers/route.php';
JModelLegacy::addIncludePath(JPATH_SITE . '/components/com_content/models', 'ContentModel');

/**
 * Helper for mod_coalawebnews
 *
 * @package     Joomla.Site
 * @subpackage  mod_coalawebnews
 *
 * @since       1.6.0
 */
abstract class ModCoalawebNewsHelper {
	/**
	 * Get a list of the latest articles from the article model
	 *
	 * @param   JRegistry  &$params  object holding the models parameters
	 *
	 * @return  mixed
	 */
	public static function getList(&$params){
		$app = JFactory::getApplication();
                $db = JFactory::getDbo();

		// Get an instance of the generic articles model
		$model = JModelLegacy::getInstance('Articles', 'ContentModel', array('ignore_request' => true));

		// Set application parameters in model
		$appParams = JFactory::getApplication()->getParams();
		$model->setState('params', $appParams);

		// Set the filters based on the module params
		$model->setState('list.start', 0);
                
                $model->setState('list.limit', (int) $params->get('count', 5) + (int) $params->get('link_count', 5));

		$model->setState('filter.published', 1);

		$model->setState('list.select', 'a.fulltext, a.id, a.title, a.alias, a.introtext, a.state, a.catid, a.created, a.created_by, a.created_by_alias,' .
			' a.modified, a.modified_by, a.publish_up, a.publish_down, a.images, a.urls, a.attribs, a.metadata, a.metakey, a.metadesc, a.access,' .
			' a.hits, a.featured, a.language' );

		// Access filter
		$access = !JComponentHelper::getParams('com_content')->get('show_noauth');
		$authorised = JAccess::getAuthorisedViewLevels(JFactory::getUser()->get('id'));
		$model->setState('filter.access', $access);

		// Category filter
		$model->setState('filter.category_id', $params->get('catid', array()));

		// Filter by language
		$model->setState('filter.language', $app->getLanguageFilter());

		// Set ordering
		$ordering = $params->get('ordering', 'a.publish_up');
		$model->setState('list.ordering', $ordering);

		if (trim($ordering) == 'rand()')
		{
			$model->setState('list.direction', '');
		}
		else
		{
			$direction = $params->get('direction', 1) ? 'DESC' : 'ASC';
			$model->setState('list.direction', $direction);
		}

		// Retrieve Content
		$items = $model->getItems();
                
                //Do we want to exclude any articles?
                $artsExclude = $params->get('articles_exclude', '');
                if (isset($artsExclude)) {
                    $artsExclude = explode(',', $artsExclude);
                    //Lets drop them out of the array before retuning the list
                    foreach($items as $elementKey => $element) {
                        foreach($element as $valueKey => $value) {
                            if($valueKey == 'id' && in_array($value, $artsExclude)){
                                //delete this particular object from the $array
                                unset($items[$elementKey]);
                            } 
                        }
                    }
                }

		foreach ($items as &$item)
		{
			$item->readmore = strlen(trim($item->fulltext));
			$item->slug = $item->id . ':' . $item->alias;
			$item->catslug = $item->catid . ':' . $item->category_alias;

			if ($access || in_array($item->access, $authorised))
			{
				// We know that user has the privilege to view the article
				$item->link = JRoute::_(ContentHelperRoute::getArticleRoute($item->slug, $item->catid));
                                $item->catLink = JRoute::_(ContentHelperRoute::getCategoryRoute($item->catid));
				$item->linkText = JText::_('MOD_CWNEWS_READMORE');
			}
			else
			{
				$item->link = JRoute::_('index.php?option=com_users&view=login');
				$item->linkText = JText::_('MOD_CWNEWS_READMORE_REGISTER');
			}
                    

		}

		return $items;
	}
        
    
    /**
     * 
     * @return boolean
     */
    public static function checkDependencies() {
        $checkOk = false;
        $minVersion = '0.1.5';
        $joomla = JFactory::getApplication();

        // Load the version.php file for the CW Gears plugin
        $version_php = JPATH_SITE . '/plugins/system/cwgears/version.php';
        if (!defined('PLG_CWGEARS_VERSION') && JFile::exists($version_php)) {
            include_once $version_php;
        }

        // Check CW Gears plugin is installed and the right version otherwise tell the user that it's needed
        $loadcount_php = JPATH_SITE . '/plugins/system/cwgears/helpers/loadcount.php';
        if (
                JPluginHelper::isEnabled('system', 'cwgears', true) == true &&
                JFile::exists($version_php) &&
                version_compare(PLG_CWGEARS_VERSION, $minVersion, 'ge') &&
                JFile::exists($loadcount_php)) {
            if (!class_exists('CwGearsHelperLoadcount')) {
                include_once $loadcount_php;
            }

            $checkOk = true;
        } else {
            if($joomla->isAdmin()){
                JFactory::getApplication()->enqueueMessage(JText::sprintf('MOD_CWNEWS_NOGEARSPLUGIN_CHECK_MESSAGE', $minVersion), 'notice');
            }
        }

        return $checkOk;
    }

}
