<?php

defined("_JEXEC") or die("Restricted access");
/**
 * @package             Joomla
 * @subpackage          CoalaWeb News Module
 * @author              Steven Palmer
 * @author url          http://coalaweb.com
 * @author email        support@coalaweb.com
 * @license             GNU/GPL, see /assets/en-GB.license.txt
 * @copyright           Copyright (c) 2016 Steven Palmer All rights reserved.
 *
 * CoalaWeb News is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

?>
<div id ="cwn-<?php echo $uniqueId; ?>" class="<?php echo $moduleclass_sfx; ?>">
        <div class="
            <?php echo $uikitPrefix; ?>-grid 
            <?php echo $uikitPrefix; ?>-grid-match
            <?php echo $uikitPrefix; ?>-margin-bottom"
            data-<?php echo $uikitPrefix; ?>-grid="{gutter: <?php echo $marginsOuter; ?>}">
            <?php
            $i = 0;
            foreach ($list as $item) :
                if ($i < $itemCount) {
                    echo '<div class="'
                    . $uikitPrefix .'-width-large-1-' .$columnsLarge . ' '
                    . $uikitPrefix .'-width-medium-1-' .$columnsMedium . ' '
                    . $uikitPrefix .'-width-small-1-' .$columnsSmall . '">';
                    
                    echo '<div class="' .$uikitPrefix . '-panel ' . $panelStyle . '">'
                    . '<div class="' .$uikitPrefix . '-grid ' .$uikitPrefix . '-grid-' . $marginsInner . '" data-' .$uikitPrefix . '-grid-margin="">';
                    
                    require JModuleHelper::getLayoutPath('mod_coalawebnews', 'default/_item');
                    
                    echo '</div></div></div>';
                }
                $i++;
            endforeach;
            ?>
        </div>
        
        <?php if ($displayLinks) {

            if ($params->get('links_title')) {
                echo '<' . $linkHeading . ' class="">' . $linkText . '</' . $linkHeading . '>';
            }

            $f = 1;
            echo '<ul class="' .$uikitPrefix . '-list">';
            foreach ($list as $item) :
                if ($f > $params->get('count')) {
                    require JModuleHelper::getLayoutPath('mod_coalawebnews', 'default/_link');
                }
                $f++;
            endforeach;
            echo '</ul>';
        }
    
        if ($moreFrom) {
            echo '<p class="' .$uikitPrefix . '-small ' .$uikitPrefix . '-text-muted">' . $morefromText . ':';
            $new_array = array();
            foreach ($list as $cat) :

                if (!array_key_exists($cat->catid, $new_array)) {
                    $new_array[$cat->catid] = $cat;
                    echo '<i class="' .$uikitPrefix . '-icon-folder-open ' .$uikitPrefix . '-margin-small-left"></i>'
                    . ' <a href="' . $cat->catLink . '">' . $cat->category_title . '</a>';
                }

            endforeach;
            echo '</p>';
        }
        ?>

    </div>
