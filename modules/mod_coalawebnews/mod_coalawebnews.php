<?php

defined("_JEXEC") or die("Restricted access");
/**
 * @package             Joomla
 * @subpackage          CoalaWeb News Module
 * @author              Steven Palmer
 * @author url          http://coalaweb.com
 * @author email        support@coalaweb.com
 * @license             GNU/GPL, see /assets/en-GB.license.txt
 * @copyright           Copyright (c) 2016 Steven Palmer All rights reserved.
 *
 * CoalaWeb News is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';

JHtml::_('jquery.framework');
    
$lang = JFactory::getLanguage();
if ($lang->getTag() != 'en-GB') {
    $lang->load('mod_coalawebnews', JPATH_SITE, 'en-GB');
}
$lang->load('mod_coalawebnews', JPATH_SITE, null, 1);

$uniqueId = $module->id;
$doc = JFactory::getDocument();

$list = ModCoalaWebNewsHelper::getList($params);


$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));
$uikitPrefix = $params->get('uikit_prefix', 'cw');

$limit = $params->get('max_char', 200);
$itemCount = $params->get('count');
$showReadmore = $params->get('show_readmore', 1);
$alwaysReadmore = $params->get('always_readmore', 0);
$readmoreText = $params->get('readmore_text', JTEXT::_('MOD_CWNEWS_BTN_RM'));
$readmoreType = $params->get('readmore_type', 'l');
$readmoreCustom = $params->get('readmore_custom');

//Remore button type
switch ($readmoreType){
    case 'p' :
        $rmTypeCw = $uikitPrefix . '-button ' .$uikitPrefix . '-button-primary';
        break;
    case 's' :
        $rmTypeCw = $uikitPrefix . '-button ' .$uikitPrefix . '-button-success';
        break;
    case 'd' :
        $rmTypeCw = $uikitPrefix . '-button ' .$uikitPrefix . '-button-danger';
        break;
    case 'l' :
        $rmTypeCw = $uikitPrefix . '-button ' .$uikitPrefix . '-button-link';
        break;
    case 'c' :
        $rmTypeCw = $readmoreCustom;
        break;
}

$displayLinks = $params->get('display_links', 0);
$displayDetails = $params->get('show_article_info');
$moreFrom = $params->get('more_from', 0);
$morefromText = $params->get('morefrom_text', JTEXT::_('MOD_CWNEWS_MORE'));
$itemHeading = $params->get('item_heading', 'h4');
$linkHeading = $params->get('link_heading', 'h4');
$linkText = $params->get('link_text', JTEXT::_('MOD_CWNEWS_LINKS'));
$showImg = $params->get('show_image', 1);
$imgType = ($params->get('image_type', 1) ? 'image_fulltext' : 'image_intro');
$imgAlt = ($params->get('image_type', 1) ? 'image_fulltext_alt' : 'image_intro_alt');
$imgCaption = ($params->get('image_type', 1) ? 'image_fulltext_caption' : 'image_intro_caption');
$imgWidthLarge = $params->get('image_width_large', 2);
$imgWidthMedium = $params->get('image_width_medium', 4);
$imgWidthSmall = $params->get('image_width_small', 10);
$columnsLarge = ($params->get('columns_large', 4) > $itemCount ? $itemCount : $params->get('columns_large', 4));
$columnsMedium = ($params->get('columns_medium', 2) > $itemCount ? $itemCount : $params->get('columns_medium', 2));
$columnsSmall = ($params->get('columns_small', 1) > $itemCount ? $itemCount : $params->get('columns_small', 1));
$artWidthLarge = ($showImg ? 10 - $imgWidthLarge : 10);
$artWidthMedium = ($showImg ? 10 - $imgWidthMedium : 10);
$artWidthSmall = ($showImg ? 10 - $imgWidthSmall : 10);
//A bit of redundancy for old settings
$marginsInner = $params->get('grid_margins_inner') === 'preserve' ? 'small' : $params->get('grid_margins_inner', 'small');
$marginsOuter = $params->get('grid_margins_outer') === 'preserve' || 'small' ? '20' : $params->get('grid_margins_outer', '20');
$textAlign = $params->get('text_align', 'justify');
$readmoreAlign = $params->get('readmore_align', 'right');
$titleAlign = $params->get('title_align', 'left');
$detailsAlign = $params->get('details_align', 'left');
$imageAlign = $params->get('image_align', 'left');
$panelType = $params->get('panel_style', 'd');
$dynFilter = $params->get('dynamic_filter', 0);
$matchHeight = $params->get('match_height', 0) ? 'data-' . $uikitPrefix . '-grid-match="{row: false}"' : '';

//Panel type
switch ($panelType){
    case 'd' :
        $panelStyle = $uikitPrefix . '-panel-box' ;
        break;
    case 'p' :
        $panelStyle = $uikitPrefix . '-panel-box ' .$uikitPrefix . '-panel-box-primary';
        break;
    case 's' :
        $panelStyle = $uikitPrefix . '-panel-box ' .$uikitPrefix . '-panel-box-secondary';
        break;
    case 'h' :
        $panelStyle = $uikitPrefix . '-panel-hover';
        break;
}

// Lets check with have our dependencies met
$checkOk = ModCoalaWebNewsHelper::checkDependencies();
if ($checkOk) {
    // Load Uikit
    $helpFunc = new CwGearsHelperLoadcount();
    $url = JURI::getInstance()->toString();
    $helpFunc::setUikitCount($url);
    $helpFunc::setUikitPlusCount($url);

}

require JModuleHelper::getLayoutPath('mod_coalawebnews', $params->get('layout', 'default'));
